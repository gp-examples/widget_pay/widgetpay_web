# widgetpay_web

## This is an example that shows how to use our widget payment in web pages.

### To use properly, follow next steps:

1. Clone this repo
2. Make a payment request (see https://support.greenpay.me/hc/es-mx/articles/360008603691-1-Crear-una-orden-de-pago-en-nuestro-API) from your backend or postman.
3. Update Update gpsesion and gptoken element values in file example.html
4. Open example.html file in your browser.